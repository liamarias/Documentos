
CREATE SEQUENCE public.personas_id_persona_seq;

CREATE TABLE public.personas (
                id_persona INTEGER NOT NULL DEFAULT nextval('public.personas_id_persona_seq'),
                nombre VARCHAR NOT NULL,
                apellido VARCHAR NOT NULL,
                cedula NUMERIC NOT NULL,
                fecha_nacimiento DATE NOT NULL,
                telefono INTEGER NOT NULL,
                ciudad VARCHAR,
                sexo VARCHAR NOT NULL,
                direccion VARCHAR,
                correo VARCHAR,
                CONSTRAINT personas_pk PRIMARY KEY (id_persona)
);


ALTER SEQUENCE public.personas_id_persona_seq OWNED BY public.personas.id_persona;

CREATE SEQUENCE public.roles_id_rol_seq;

CREATE TABLE public.roles (
                id_rol INTEGER NOT NULL DEFAULT nextval('public.roles_id_rol_seq'),
                nombre_rol VARCHAR NOT NULL,
                descripcion VARCHAR,
                usr_alta VARCHAR NOT NULL,
                fec_alta DATE NOT NULL,
                fec_ult_act DATE NOT NULL,
                CONSTRAINT roles_pk PRIMARY KEY (id_rol)
);


ALTER SEQUENCE public.roles_id_rol_seq OWNED BY public.roles.id_rol;

CREATE TABLE public.usuarios (
                usuario VARCHAR NOT NULL,
                estado INTEGER NOT NULL,
                password VARCHAR NOT NULL,
                id_rol INTEGER NOT NULL,
                usr_alta VARCHAR NOT NULL,
                fec_alta DATE NOT NULL,
                fec_ult_act DATE NOT NULL,
                CONSTRAINT usuarios_pk PRIMARY KEY (usuario)
);


CREATE SEQUENCE public.clientes_id_cliente_seq;

CREATE TABLE public.clientes (
                id_cliente INTEGER NOT NULL DEFAULT nextval('public.clientes_id_cliente_seq'),
                razon_social VARCHAR NOT NULL,
                ruc VARCHAR NOT NULL,
                direccion VARCHAR NOT NULL,
                telefono VARCHAR NOT NULL,
                usuario VARCHAR NOT NULL,
                id_persona INTEGER NOT NULL,
                CONSTRAINT clientes_pk PRIMARY KEY (id_cliente)
);


ALTER SEQUENCE public.clientes_id_cliente_seq OWNED BY public.clientes.id_cliente;

CREATE SEQUENCE public.proyectos_id_proyecto_seq;

CREATE TABLE public.proyectos (
                id_proyecto INTEGER NOT NULL DEFAULT nextval('public.proyectos_id_proyecto_seq'),
                nombre_proyecto VARCHAR NOT NULL,
                descripcion VARCHAR,
                id_cliente INTEGER NOT NULL,
                fecha_inicio DATE,
                fecha_fin DATE,
                estado VARCHAR NOT NULL,
                CONSTRAINT proyectos_pk PRIMARY KEY (id_proyecto)
);


ALTER SEQUENCE public.proyectos_id_proyecto_seq OWNED BY public.proyectos.id_proyecto;

CREATE SEQUENCE public.empleados_id_empleado_seq;

CREATE TABLE public.empleados (
                id_empleado INTEGER NOT NULL DEFAULT nextval('public.empleados_id_empleado_seq'),
                fecha_ingreso DATE NOT NULL,
                cargo VARCHAR NOT NULL,
                usuario VARCHAR NOT NULL,
                estado VARCHAR NOT NULL,
                id_persona INTEGER NOT NULL,
                CONSTRAINT empleados_pk PRIMARY KEY (id_empleado)
);


ALTER SEQUENCE public.empleados_id_empleado_seq OWNED BY public.empleados.id_empleado;

CREATE TABLE public.tareas (
                id_proyecto INTEGER NOT NULL,
                id_tarea INTEGER NOT NULL,
                descripcion VARCHAR NOT NULL,
                id_empleado INTEGER NOT NULL,
                estado VARCHAR NOT NULL,
                fecha_inicio DATE,
                prioridad INTEGER DEFAULT 10 NOT NULL,
                fecha_fin DATE,
                user_alta VARCHAR NOT NULL,
                CONSTRAINT tareas_pk PRIMARY KEY (id_proyecto, id_tarea)
);


CREATE TABLE public.equipos (
                id_proyecto INTEGER NOT NULL,
                id_empleado INTEGER NOT NULL,
                descripcion VARCHAR NOT NULL,
                estado VARCHAR NOT NULL,
                usr_alta VARCHAR NOT NULL,
                fec_alta DATE NOT NULL,
                fec_ult_act DATE NOT NULL,
                CONSTRAINT equipos_pk PRIMARY KEY (id_proyecto, id_empleado)
);


ALTER TABLE public.empleados ADD CONSTRAINT personas_empleados_fk
FOREIGN KEY (id_persona)
REFERENCES public.personas (id_persona)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.clientes ADD CONSTRAINT personas_clientes_fk
FOREIGN KEY (id_persona)
REFERENCES public.personas (id_persona)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.usuarios ADD CONSTRAINT roles_usuarios_fk
FOREIGN KEY (id_rol)
REFERENCES public.roles (id_rol)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.empleados ADD CONSTRAINT usuarios_empleados_fk
FOREIGN KEY (usuario)
REFERENCES public.usuarios (usuario)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.clientes ADD CONSTRAINT usuarios_clientes_fk
FOREIGN KEY (usuario)
REFERENCES public.usuarios (usuario)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.proyectos ADD CONSTRAINT clientes_proyectos_fk
FOREIGN KEY (id_cliente)
REFERENCES public.clientes (id_cliente)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.tareas ADD CONSTRAINT proyectos_tareas_fk
FOREIGN KEY (id_proyecto)
REFERENCES public.proyectos (id_proyecto)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.equipos ADD CONSTRAINT proyectos_equipos_fk
FOREIGN KEY (id_proyecto)
REFERENCES public.proyectos (id_proyecto)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.equipos ADD CONSTRAINT empleados_equipos_fk
FOREIGN KEY (id_empleado)
REFERENCES public.empleados (id_empleado)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.tareas ADD CONSTRAINT empleados_tareas_fk
FOREIGN KEY (id_empleado)
REFERENCES public.empleados (id_empleado)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;
